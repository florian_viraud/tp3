class Box:
    def __init__(self,is_open = True,capacity = None):
        self._contents = []
        self._ouvert = is_open
        self._capacity = capacity

    def add(self,truc):
        self._contents.append(truc)

    def __contains__(self,machin):
        return machin in self._contents

    def remove(self,truc):
        self._contents.remove(truc)

    def is_open(self):
        return self._ouvert

    def close(self):
        self._ouvert = False

    def open(self):
        self._ouvert = True

    def action_look(self):
        if not self.is_open():
            return "la boite est fermée"
        else:
            return "la boite contient: " + ", ".join(self._contents)

    def set_capacity(self,capacity):
        self._capacity = capacity

    def get_capacity(self):
        return self._capacity

    def has_room_for(self,truc):
        return self._capacity == None or truc.get_volume() <= self._capacity

    def action_add(self,truc):
        if self.is_open():
            if self.has_room_for(truc):
                self.add(truc)
                self._capacity -= truc.get_volume()
                return True
        return False

    def find(self,truc):
        for trucs in self._contents:
            if trucs.get_name() == truc:
                return trucs
        return None
