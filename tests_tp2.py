from Box import *
from Thing import *

def test_box_create():
  b = Box()

def test_box_contains():
    b = Box()
    b.add("truc")
    b.add("machin")
    assert "truc" in b
    assert "bidule" not in b
    assert "machin" in b

def test_box_remove():
    b = Box()
    b.add("truc")
    b.add("machin")
    b.remove("truc")
    assert "machin" in b
    assert not "truc" in b

def test_box_open():
    b = Box(is_open = False)
    assert not b.is_open()
    b.open()
    assert b.is_open()
    b.close()
    assert not b.is_open()

def test_box_look():
    b = Box()
    b.add("truc")
    b.add("machin")
    b.close()
    assert b.action_look() == "la boite est fermée"
    b.open()
    assert b.action_look() == "la boite contient: truc, machin"

def test_thing_create():
    t = Thing(3,"truc")
    assert t.get_volume() == 3
    assert t.get_name() == "truc"
    assert t.has_name("truc")

def test_box_capacity():
    b = Box()
    assert b.get_capacity() == None
    b.set_capacity(5)
    assert b.get_capacity() == 5

def test_box_room_for():
    b = Box()
    t = Thing(3,"truc")
    m = Thing(1,"machin")
    bid = Thing(6,"bidule")
    b.set_capacity(5)
    assert b.has_room_for(m)
    assert not b.has_room_for(bid)

def test_box_action_add():
    b = Box()
    t = Thing(3,"truc")
    m = Thing(1,"machin")
    bid = Thing(6,"bidule")
    b.set_capacity(5)
    assert b.action_add(t)
    assert not b.action_add(bid)

def test_repr():
    t = Thing(12,"Truc")
    assert str(t) == "Thing(12,Truc)"

def test_find():
    b = Box()
    t = Thing(10,"Truc")
    bid = Thing(4,"Bidule")
    b.add(t)
    b.add(bid)
    assert b.find("Truc")
    assert b.find("Bidule")

def test_name_none():
    t = Thing(3)
    assert t.has_name(None)
    b = Thing(4,name = "Bidule")
    assert b.has_name("Bidule")

def test_box_none():
    b = Box(capacity = 5)
    assert b.get_capacity() == 5
    assert b.is_open()
